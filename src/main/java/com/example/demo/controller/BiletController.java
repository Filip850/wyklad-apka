package com.example.demo.controller;

import com.example.demo.model.Bilet;
import com.example.demo.service.BiletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bilet")
public class BiletController {

    public BiletService biletService;

    @Autowired
    public BiletController(BiletService biletService) {
        this.biletService = biletService;
    }

    @GetMapping
    public List<Bilet> getTickets(){
        List<Bilet> tickets = biletService.getTickets();

        return tickets;
    }

    @PostMapping()
    public void addTicket(@RequestParam(name = "filmName") String filmName){
        Bilet b = new Bilet(filmName);

        biletService.addTicket(b);
    }

}
