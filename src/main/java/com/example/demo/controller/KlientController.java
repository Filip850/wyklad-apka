package com.example.demo.controller;


import com.example.demo.model.Klient;
import com.example.demo.service.KlientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/klient")
public class KlientController {

    private KlientService klientService;

    @Autowired
    public KlientController(KlientService klientService) {
        this.klientService = klientService;
    }

    @PostMapping()
    public void newUser(@RequestParam(name = "name") String name,
                        @RequestParam(name = "subname") String nazwisko){
        klientService.addNewUser(name, nazwisko);
    }

    @GetMapping()
    public List<Klient> getUsers(){
        return klientService.getUsers();
    }

    @GetMapping("/{UserID}")
    public Klient getUser(@PathVariable(name = "UserID") Long id){
//        Long id = Long.valueOf(idString);
        return klientService.getUserById(id);
    }
}
