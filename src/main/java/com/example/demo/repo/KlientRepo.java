package com.example.demo.repo;

import com.example.demo.model.Klient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KlientRepo extends JpaRepository<Klient, Long> {
}
