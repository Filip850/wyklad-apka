package com.example.demo.repo;

import com.example.demo.model.Bilet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface BiletRepo extends JpaRepository<Bilet, Long> {
}
