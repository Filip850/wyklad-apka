package com.example.demo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Seans {

    public Seans(String filmName) {
        this.filmName = filmName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String filmName;



    @OneToOne(cascade = CascadeType.PERSIST)
    private Sala sala;
}
