package com.example.demo.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Bilet {

    public Bilet(String filmName) {
        newSeans(filmName);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @OneToOne(cascade = CascadeType.PERSIST)
    private Seans seans;

    public void newSeans(String filmName){
        Seans s = new Seans(filmName);
        this.seans = s;
    }
}
