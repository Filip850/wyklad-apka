package com.example.demo.service;

import com.example.demo.model.Klient;
import com.example.demo.repo.KlientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class KlientService {

    private KlientRepo klientRepo;

    @Autowired
    public KlientService(KlientRepo klientRepo) {
        this.klientRepo = klientRepo;
    }

    public void addNewUser(String imie, String nazwisko){
        Klient k = new Klient(imie, nazwisko);

        klientRepo.save(k);
    }

    public List<Klient> getUsers(){
        return klientRepo.findAll();
    }

    public Klient getUserById(Long id){
        Optional<Klient> k = klientRepo.findById(id);
        return k.orElse(null);
    }
}
