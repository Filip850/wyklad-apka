package com.example.demo.service;

import com.example.demo.model.Bilet;
import com.example.demo.repo.BiletRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BiletService {

    private BiletRepo biletRepo;

    @Autowired
    public BiletService(BiletRepo biletRepo) {
        this.biletRepo = biletRepo;
    }

    public List<Bilet> getTickets(){
        return biletRepo.findAll();
    }

    public void addTicket(Bilet bilet){
        biletRepo.save(bilet);
    }
}
